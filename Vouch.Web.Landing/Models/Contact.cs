﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.ComponentModel.DataAnnotations;

namespace Vouch.Web.Landing.Models
{
	public class Contact: TableEntity
	{
        [Required(ErrorMessage = "An email address is required"), EmailAddress]
		public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public string RegistrationIP { get; set; }

        public string Name
		{
			get;
			set;
		}
		public string Company
		{
			get;
			set;
		}

        public Contact()
        {
            RowKey = Guid.NewGuid().ToString();
            PartitionKey = "Contact";
        }
	}
}

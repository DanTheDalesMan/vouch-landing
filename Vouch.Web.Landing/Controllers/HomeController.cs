﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Vouch.Web.Landing.Models;

namespace Vouch.Web.Landing.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			var mvcName = typeof(Controller).Assembly.GetName();
			var isMono = Type.GetType("Mono.Runtime") != null;

			ViewData["Version"] = mvcName.Version.Major + "." + mvcName.Version.Minor;
			ViewData["Runtime"] = isMono ? "Mono" : ".NET";

			return View(new Contact());
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<PartialViewResult> Submit(Contact model)
		{
			bool isMessageSent = true;

			if (ModelState.IsValid)
			{
				try
				{
                    model.RegisteredAt = DateTime.Now;
                    model.RegistrationIP = GetIPAddress();
					var email = new MailAddress(WebConfigurationManager.AppSettings["SiteEmail"]);
					StringBuilder sb = new StringBuilder();
					sb.AppendLine("Somone has registered interest on the website");
					sb.AppendLine(string.Format("Regsitered email: {0} registered at {1}", model.Email, model.RegisteredAt.ToString("dd/MM/yy hh:mm")));
					SendGrid.SendGridMessage msg = new SendGrid.SendGridMessage(email, new MailAddress[] { email }, "Vouch: Someone is intrigued", "", sb.ToString());
					SendGrid.Web sw = new SendGrid.Web(new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["sgUsername"], WebConfigurationManager.AppSettings["sgPassword"]));

					await sw.DeliverAsync(msg);
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("AzureStorageConnectionString"));
                    CloudTableClient client = storageAccount.CreateCloudTableClient();
                    CloudTable table = client.GetTableReference("VouchInterest");
                    table.CreateIfNotExists();
                    var insertOp = TableOperation.Insert(model);
                    await table.ExecuteAsync(insertOp);
                }
				catch (Exception ex)
				{
					Debug.Write(ex.ToString());
					isMessageSent = false;
                    try
                    {
                        var email = new MailAddress(WebConfigurationManager.AppSettings["GeekEmail"]);
                        var sb = new StringBuilder();
                        sb.AppendLine("An error occured when interest was registered");
                        sb.AppendLine();
                        sb.AppendLine(ex.ToString());
                        sb.AppendLine();
                        sb.AppendLine("Model data:");
                        sb.AppendLine(JsonConvert.SerializeObject(model));
                        SendGrid.SendGridMessage msg = new SendGrid.SendGridMessage(email, new MailAddress[] { email }, "Vouch: error on site", "", sb.ToString());
                        SendGrid.Web sw = new SendGrid.Web(new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["sgUsername"], WebConfigurationManager.AppSettings["sgPassword"]));
                    }
                    catch(Exception exception)
                    {
                        
                    }
				}
			}
			else
			{
				isMessageSent = false;
			}
			return PartialView("_SubmitMessage", isMessageSent);
		}

        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}

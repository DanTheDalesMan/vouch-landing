﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Vouch.Web.Landing.Models;

namespace Vouch.Web.Landing.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			var mvcName = typeof(Controller).Assembly.GetName();
			var isMono = Type.GetType("Mono.Runtime") != null;

			ViewData["Version"] = mvcName.Version.Major + "." + mvcName.Version.Minor;
			ViewData["Runtime"] = isMono ? "Mono" : ".NET";

			return View(new Contact());
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<PartialViewResult> Submit(Contact model)
		{
			bool isMessageSent = true;

			if (ModelState.IsValid)
			{
				try
				{
					//await EmailService.SendContactForm(model);
					await Task.Run(() =>
					{
					});
				}
				catch (Exception ex)
				{
					Debug.Write(ex.ToString());
					isMessageSent = false;
				}
			}
			else
			{
				isMessageSent = false;
			}
			return PartialView("_SubmitMessage", isMessageSent);
		}
	}
}

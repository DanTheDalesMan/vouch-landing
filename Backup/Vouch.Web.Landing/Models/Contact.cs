﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Vouch.Web.Landing.Models
{
	public class Contact
	{
		[Required(ErrorMessage = "An email address is required"), EmailAddress]
		public string Email { get; set; }
		public string Name
		{
			get;
			set;
		}
		public string Company
		{
			get;
			set;
		}
	}
}
